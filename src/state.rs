use crate::{api_data, StopID, UserID};
use chrono::Duration;
use geo::{point, Closest, ClosestPoint, CrossTrackDistance, Line, LineString};

use rstar::{RTree, RTreeObject, AABB};
use std::collections::HashSet;
use std::hash::{Hash, Hasher};
use std::sync::Arc;

#[derive(Debug)]
pub struct BusProgress {
    notified_subscribers: HashSet<UserID>,
    pending_stops: Vec<StopID>,
    last_completed_stop: StopID,
    current_segment: Line<f64>,
    pending_segments: LineString<f64>,
    completed_segments: LineString<f64>,
    trip: Arc<Trip>,
}

impl BusProgress {
    pub fn new(trip: Arc<Trip>) -> Self {
        Self {
            notified_subscribers: HashSet::new(),
            last_completed_stop: trip
                .stops
                .first()
                .map(|stop| stop.id.clone())
                .unwrap(),
            current_segment: trip.path.lines().next().unwrap(),
            pending_segments: trip.path.clone(),
            pending_stops: trip
                .stops
                .iter()
                .map(|stop| stop.id.clone())
                .collect(),
            trip: trip.clone(),
            completed_segments: LineString::new(vec![]),
        }
    }

    pub fn update_position(&mut self, lon: f64, lat: f64) {
        let point = point!(x: lon, y: lat);

        // Take care. The following code has a mixture of spherical and euclidian distance
        // It should do for now but the code might need to be revised later.

        let mut nearest_segment = self.current_segment.clone();
        let mut nearest_index = 0;

        let mut nearest_distance = point.cross_track_distance(
            &nearest_segment.start.into(),
            &nearest_segment.end.into(),
        );

        let mut nearest_point = match nearest_segment.closest_point(&point) {
            Closest::Intersection(p) => p,
            Closest::SinglePoint(p) => p,
            Closest::Indeterminate => {
                dbg!(point,nearest_segment);
                unreachable!("Indeterminate closest point")
            }
        };
        let mut changed = false;

        for (i, segment) in self.pending_segments.lines().enumerate() {
            let segment_distance = point.cross_track_distance(
                &nearest_segment.start.into(),
                &nearest_segment.end.into(),
            );
            if segment_distance < nearest_distance {
                nearest_distance = segment_distance;
                nearest_segment = segment;
                nearest_index = i;
                changed = true;
            }

            nearest_point = match nearest_segment.closest_point(&point) {
                Closest::Intersection(p) => p,
                Closest::SinglePoint(p) => p,
                Closest::Indeterminate => {
                    unreachable!("Indeterminate closest point")
                }
            };
        }

        let completed_segments = LineString::new(
            self.trip
                .path
                .coords()
                .cloned()
                .take(nearest_index)
                .chain([nearest_point.into()].into_iter())
                .collect::<Vec<_>>(),
        );

        self.completed_segments = completed_segments;
    }
}

#[derive(Debug, Clone)]
pub struct Trip {
    pub(crate) id: String,
    pub(crate) headsign: String,
    pub(crate) stops: Vec<Arc<Stop>>,
    pub(crate) stop_tree: RTree<Stop>,
    pub(crate) path: LineString<f64>,
}

impl PartialEq for Trip {
    fn eq(&self, other: &Self) -> bool {
        self.stops == other.stops && self.id == other.id
    }
}

impl Eq for Trip {}

impl Hash for Trip {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.stops.hash(state);
    }
}

// impl Trip {
//     fn new_from(gtfs_trip: GtfsTripEntry, stops: &HashMap<String, Stop>) -> Self {
//         gtfs_trip.
//         Self {
//             headsign: entry.trip_headsign.clone(),
//             stops,
//             stop_tree,
//             path,
//         }
//     }
// }

#[derive(Debug, Clone)]
pub struct Stop {
    id: StopID,
    lat: f64,
    lon: f64,
}

impl Hash for Stop {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

impl From<api_data::Stop> for Stop {
    fn from(stop: api_data::Stop) -> Self {
        Self {
            id: stop.stop_id.parse::<_>().unwrap(),
            lat: stop.stop_lat,
            lon: stop.stop_lon,
        }
    }
}

impl RTreeObject for Stop {
    type Envelope = AABB<[f64; 2]>;

    fn envelope(&self) -> Self::Envelope {
        AABB::from_point([self.lon, self.lat])
    }
}

impl PartialEq for Stop {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

struct StopSubscribers {
    distance_subscribers: Vec<(u64, UserID)>,
    time_subscribers: Vec<(Duration, UserID)>,
}

impl StopSubscribers {
    fn new() -> Self {
        Self {
            distance_subscribers: vec![],
            time_subscribers: vec![],
        }
    }

    fn add_distance_subscriber(&mut self, distance: u64, user_id: UserID) {
        self.distance_subscribers.push((distance, user_id));
    }

    fn add_time_subscriber(&mut self, time: Duration, user_id: UserID) {
        self.time_subscribers.push((time, user_id));
    }

    fn distance_trigger(&self, distance: u64) -> Vec<UserID> {
        let to_be_notified_uuids = self
            .distance_subscribers
            .iter()
            .take_while(|(subscriber_distance, _subscriber_uid)| {
                subscriber_distance < &distance
            })
            .map(|(_, subscriber_uid)| subscriber_uid.clone())
            .collect();
        to_be_notified_uuids
    }

    fn time_trigger(&self, duration: Duration) -> Vec<UserID> {
        let to_be_notified_uuids = self
            .time_subscribers
            .iter()
            .take_while(|(subscriber_duration, _subscriber_uid)| {
                subscriber_duration < &duration
            })
            .map(|(_, subscriber_uid)| subscriber_uid.clone())
            .collect::<Vec<UserID>>();
        to_be_notified_uuids
    }
}
