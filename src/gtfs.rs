use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::sync::Arc;
use std::{fs, io};

use geo::{coord, LineString};
use itertools::Itertools;
use rstar::RTree;
use serde::{Deserialize, Serialize};

use crate::errors::Error;
use crate::{state, StopID};

#[derive(Debug, Serialize, Deserialize)]
pub struct GtfsShapeEntry {
    pub shape_id: String,
    pub shape_pt_lat: f64,
    pub shape_pt_lon: f64,
    pub shape_pt_sequence: u32,
}

pub(crate) fn read_shapes() -> HashMap<String, LineString<f64>> {
    let f = fs::File::open("./cache/shapes.txt").unwrap();
    let reader = io::BufReader::new(f);

    let mut rdr = csv::ReaderBuilder::new()
        .trim(csv::Trim::All)
        .from_reader(reader);

    let shape_entries = rdr
        .deserialize()
        .into_iter()
        .map(|result| result.unwrap())
        .collect::<Vec<GtfsShapeEntry>>();

    shape_entries
        .into_iter()
        .group_by(|entry| entry.shape_id.clone())
        .into_iter()
        .map(|(shape_id, group)| {
            (shape_id,
             LineString::new(
                 group.map(|entry|
                     coord! {x: entry.shape_pt_lon, y: entry.shape_pt_lat})
                     // TODO dedup added here @Claudio can there be issues?
                     .dedup()
                     .collect()
             )
            )
        })
        .collect::<HashMap<_, _>>()
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GtfsTripEntry {
    pub route_id: String,
    pub trip_id: String,
    pub shape_id: String,
    pub trip_headsign: String,
}

pub(crate) fn read_trips() -> HashMap<String, GtfsTripEntry> {
    let f = fs::File::open("./cache/trips.txt").unwrap();
    let reader = io::BufReader::new(f);

    let mut rdr = csv::ReaderBuilder::new()
        .trim(csv::Trim::All)
        .from_reader(reader);

    rdr.deserialize()
        .into_iter()
        .map(|result| {
            let result: GtfsTripEntry = result.unwrap();
            (result.trip_id.clone(), result)
        })
        .collect::<HashMap<_, _>>()
}

fn tml_gtfs_trips() -> Vec<GtfsTripEntry> {
    let f = fs::File::open("cache/trips.txt").unwrap();
    let reader = io::BufReader::new(f);

    let mut rdr = csv::ReaderBuilder::new()
        // .trim(csv::Trim::All)
        .from_reader(reader);

    rdr.deserialize()
        .into_iter()
        .map(|result| result.unwrap())
        .collect::<Vec<GtfsTripEntry>>()
}

#[derive(Serialize, Deserialize)]
pub struct GTFSStopSequenceId {
    pub trip_id: String,
    pub stop_id: u32,
    pub stop_sequence: u32,
}

fn load_gtfs_stop_times() -> Vec<GTFSStopSequenceId> {
    let f = fs::File::open("cache/stop_times.txt").unwrap();
    let reader = io::BufReader::new(f);

    let mut rdr = csv::ReaderBuilder::new()
        // .trim(csv::Trim::All)
        .from_reader(reader);

    rdr.deserialize()
        .into_iter()
        .map(|result| result.unwrap())
        .collect::<Vec<GTFSStopSequenceId>>()
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GTFSRoute {
    pub route_id: String,
    pub route_short_name: String,
    pub route_long_name: String,
}

fn tml_gtfs_routes() -> Vec<GTFSRoute> {
    let f = fs::File::open("cache/routes.txt").unwrap();
    let reader = io::BufReader::new(f);

    let mut rdr = csv::ReaderBuilder::new()
        // .trim(csv::Trim::All)
        .from_reader(reader);

    rdr.deserialize()
        .into_iter()
        .map(|result| result.unwrap())
        .collect::<Vec<GTFSRoute>>()
}

#[derive(Debug)]
pub struct BusRoute {
    pub(crate) id: String,
    pub(crate) name: String,
    pub(crate) trips: Vec<state::Trip>,
}

// #[derive(Debug, Eq, Clone, Serialize, Deserialize)]
// pub struct RoutePath {
//     id: String,
//     headsign: String,
//     stops: Vec<u32>,
// }
// impl PartialEq for RoutePath {
//     fn eq(&self, other: &Self) -> bool {
//         // self.id == other.id
//         self.stops == other.stops && self.headsign == other.headsign
//     }
// }

// impl Hash for RoutePath {
//     fn hash<H: Hasher>(&self, state: &mut H) {
//         // self.id.hash(state);
//         self.headsign.hash(state);
//         self.stops.hash(state);
//     }
// }

pub fn calculate_gtfs_stop_sequence(
    gtfs_stop_times: &Vec<GTFSStopSequenceId>,
) -> HashMap<String, Vec<u32>> {
    gtfs_stop_times
        .into_iter()
        .into_group_map_by(|x| &x.trip_id)
        .into_iter()
        .map(|(trip_id, stop_times)| {
            let stop_ids = stop_times
                .into_iter()
                .sorted_by_key(|stop_time| stop_time.stop_sequence)
                .map(|stop_time| stop_time.stop_id)
                .collect::<Vec<_>>();

            (trip_id.clone(), stop_ids)
        })
        .collect::<HashMap<_, _>>()
}

pub(crate) fn tml_gtfs_route_trips(
    stops: &HashMap<StopID, Arc<state::Stop>>,
    shapes: &HashMap<String, LineString<f64>>,
) -> Result<HashMap<String, BusRoute>, Error> {
    let gtfs_stop_times = load_gtfs_stop_times();
    let gtfs_trips = tml_gtfs_trips();
    let gtfs_routes = tml_gtfs_routes();

    let gtfs_route_names = gtfs_routes
        .into_iter()
        .map(|route| (route.route_id, route.route_long_name))
        .collect::<HashMap<String, String>>();

    let trips_stop_seq = calculate_gtfs_stop_sequence(&gtfs_stop_times);

    gtfs_trips
        .into_iter()
        .into_group_map_by(|trip| trip.route_id.clone())
        .into_iter()
        .map(|(route_id, trip_refs)| {
            Ok((
                route_id.clone(),
                BusRoute {
                    id: route_id.clone(),
                    name: gtfs_route_names
                        .get(&route_id)
                        .cloned()
                        .unwrap_or_default(),
                    trips: trip_refs
                        .into_iter()
                        .map(|trip| {
                            build_trips(trip, &trips_stop_seq, stops, shapes)
                        })
                        .unique()
                        .collect::<Result<Vec<state::Trip>, Error>>()?,
                },
            ))
        })
        .collect::<Result<HashMap<String, BusRoute>, Error>>()
}

fn build_trips(
    trip_ref: GtfsTripEntry,
    trips_stop_seq: &HashMap<String, Vec<u32>>,
    stops: &HashMap<StopID, Arc<state::Stop>>,
    shapes: &HashMap<String, LineString<f64>>,
) -> Result<state::Trip, Error> {
    let stop_seq = trips_stop_seq
        .get(&trip_ref.trip_id)
        .ok_or(Error::TripNotFound)?;

    let trip_stops = stop_seq
        .iter()
        .map(|stop_id| {
            stops
                .get(&stop_id)
                .map(|stop| stop.clone())
                .ok_or(Error::StopNotFound)
        })
        .collect::<Result<Vec<Arc<state::Stop>>, Error>>()?;

    let stop_tree = RTree::bulk_load(
        trip_stops
            .iter()
            .map(|stop| state::Stop::clone(stop))
            .collect_vec(),
    );

    let path = shapes
        .get(&trip_ref.shape_id)
        .ok_or(Error::ShapeNotFound)?
        .clone();

    Ok(state::Trip {
        id: trip_ref.trip_id,
        headsign: trip_ref.trip_headsign,
        stops: trip_stops,
        stop_tree,
        path,
    })
}
