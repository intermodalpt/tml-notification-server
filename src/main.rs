/*
    Copyright (C) 2023  Cláudio Pereira

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::collections::{HashMap, HashSet};
use std::io::BufReader;
use std::path::PathBuf;
use std::sync::{Arc, RwLock};

use chrono::Weekday;
use chrono::{
    DateTime, Datelike, Duration, LocalResult, TimeZone, Timelike, Utc,
};
use config::Map;
use geo::ClosestPoint;
use itertools::Itertools;
use rand::prelude::{IteratorRandom, SliceRandom, SmallRng};
use rand::{Rng, RngCore, SeedableRng};
use rayon::iter::ParallelIterator;
use rayon::iter::{IntoParallelIterator, IntoParallelRefIterator};
use serde::de::DeserializeOwned;
use serde::Serialize;
use sha1::{Digest, Sha1};

use api_data::{StopPrediction, VehicleEvent};
use state::BusProgress;

use crate::gtfs::{read_shapes, tml_gtfs_route_trips};
use crate::state::Trip;

mod api_data;
mod errors;
mod gtfs;
mod state;

pub(crate) type AppState = Arc<State>;

pub type UserID = String;
pub type StopID = u32;
pub type RouteID = String;

type UserPreference = Vec<Preference>;
type PositionPreference = (u32, PreferenceData);
type PredictionPreference = (Duration, PreferenceData);

static SEED: u64 = 123456789;

fn get_random_weekdays(rng: &mut SmallRng) -> Vec<Weekday> {
    let mut weekdays = vec![];

    if rng.next_u32() % 2 == 0 {
        weekdays.push(Weekday::Mon);
    }
    if rng.next_u32() % 2 == 0 {
        weekdays.push(Weekday::Tue);
    }
    if rng.next_u32() % 2 == 0 {
        weekdays.push(Weekday::Wed);
    }
    if rng.next_u32() % 2 == 0 {
        weekdays.push(Weekday::Thu);
    }
    if rng.next_u32() % 2 == 0 {
        weekdays.push(Weekday::Fri);
    }
    if rng.next_u32() % 2 == 0 {
        weekdays.push(Weekday::Sat);
    }
    if rng.next_u32() % 2 == 0 {
        weekdays.push(Weekday::Sun);
    }

    weekdays
}

#[derive(Debug, Clone)]
struct PreferenceData {
    stop_id: StopID,
    route_id: RouteID,
    days: HashSet<Weekday>,
    start_time: DateTime<Utc>,
    duration: Duration,
}

impl PreferenceData {
    fn gen_random(
        rng: &mut SmallRng,
        stop_routes: &HashMap<StopID, Vec<RouteID>>,
    ) -> Self {
        // let days = get_random_weekdays(rng).into_iter().collect();
        let days = HashSet::new();
        let start_time = Utc::now();
        let duration = Duration::minutes(30);

        // let routes = routes
        //     .get(&stop_id)
        //     .expect("Tens um bug ó parvo!")
        //     .iter()
        //     .choose(&mut rng)
        //     .expect("Olha outro! Olha outro!")
        //     .clone();

        // let line = routes
        //     .iter()
        //     .filter(|(_, routes)| !routes.is_empty())
        //     .choose(rng)
        //     .expect("No lines exist?");

        let randomly_selected_stop = stop_routes
            .iter()
            .filter(|(_, routes)| !routes.is_empty())
            .cycle()
            .skip(rng.next_u32() as usize % stop_routes.len())
            .next()
            .expect("No stops exist?");

        let randomly_selected_route_id = randomly_selected_stop
            .1
            .iter()
            .choose(rng)
            .expect("No routes available")
            .clone();

        Self {
            stop_id: randomly_selected_stop.0.clone(),
            route_id: randomly_selected_route_id,
            days,
            start_time,
            duration,
        }
    }
}

#[derive(Debug)]
enum Preference {
    Position {
        data: PreferenceData,
        distance: u32,
    },
    Prediction {
        data: PreferenceData,
        duration: Duration,
    },
}

impl Preference {
    fn gen_n_random(
        n: usize,
        rng: &mut SmallRng,
        stop_routes: &HashMap<StopID, Vec<RouteID>>,
    ) -> Vec<Preference> {
        (1..n)
            .into_iter()
            .map(|i| {
                if i % 2 == 0 {
                    Preference::Position {
                        data: PreferenceData::gen_random(rng, stop_routes),
                        distance: 1000,
                    }
                } else {
                    Preference::Prediction {
                        data: PreferenceData::gen_random(rng, stop_routes),
                        duration: Duration::minutes(30),
                    }
                }
            })
            .collect()
    }
}

struct State {
    user_preferences: RwLock<HashMap<UserID, Arc<UserPreference>>>,
    position_map: RwLock<Map<(String, Weekday, u32), Vec<PositionPreference>>>,
    prediction_map:
        RwLock<Map<(String, Weekday, u32), Vec<PredictionPreference>>>,
    bus_progress: RwLock<HashMap<String, BusProgress>>,
}

mod server_data {
    use std::collections::HashMap;

    use serde::{Deserialize, Serialize};

    use super::StopID;

    #[derive(Serialize, Deserialize, Debug)]
    struct StopPreview {
        #[serde(rename = "journeyId")]
        pub journey_id: String,
        #[serde(rename = "estimatedArrivalTime")]
        pub estimated_arrival_time: Option<String>,
        #[serde(rename = "estimatedDepartureTime")]
        pub estimated_departure_time: Option<String>,
        #[serde(rename = "observedArrivalTime")]
        pub observed_arrival_time: Option<String>,
        #[serde(rename = "observedDepartureTime")]
        pub observed_departure_time: Option<String>,
        #[serde(rename = "observedVehicleId")]
        pub observed_vehicle_id: Option<String>,
        #[serde(rename = "lineId")]
        pub line_id: String,
        #[serde(rename = "patternId")]
        pub pattern_id: String,
        #[serde(rename = "timetabledArrivalTime")]
        pub timetabled_arrival_time: String,
        #[serde(rename = "timetabledDepartureTime")]
        pub timetabled_departure_time: String,
    }

    struct StopCache {
        previews: HashMap<StopID, StopPreview>,
    }
}

// pub(crate) fn build_paths(state: AppState) -> Router {
//     let cors = CorsLayer::new()
//         .allow_methods([
//             Method::HEAD,
//             Method::GET,
//             Method::POST,
//             Method::PATCH,
//             Method::DELETE,
//         ])
//         // .allow_headers([header::CONTENT_TYPE, header::AUTHORIZATION])
//         .allow_headers(Any)
//         .allow_origin(Any);
//
//     Router::new()
//         .route(
//             "/v1/tml/match/:stop_id/:gtfs_id",
//             post(gtfs::handlers::tml_match_stop),
//         )
//         .with_state(state)
//         .layer(DefaultBodyLimit::disable())
//         .layer(RequestBodyLimitLayer::new(30 * 1024 * 1024 /* 30mb */))
//         .layer(cors)
// }

async fn fetch_if_not_cached<E>(
    url: &str,
) -> Result<E, Box<dyn std::error::Error>>
where
    E: DeserializeOwned + Serialize,
{
    let mut hasher = Sha1::new();
    hasher.update(&url.as_bytes());
    let hash = hasher.finalize();
    let hex_hash = base16ct::lower::encode_string(&hash);

    let path = PathBuf::from("cache").join(hex_hash);
    let data = if path.exists() {
        let file = std::fs::File::open(path)?;
        let reader = BufReader::new(file);
        let resp = serde_json::from_reader(reader)?;
        println!("Pog");
        resp
    } else {
        println!("Pepega");
        let data = reqwest::get(url).await?.json::<E>().await?;
        // Create the folder in case it does not exist
        std::fs::create_dir_all("cache")?;
        let file = std::fs::File::create(path)?;
        let writer = std::io::BufWriter::new(file);
        serde_json::to_writer(writer, &data)?;
        data
    };
    Ok(data)
}

impl VehicleEvent {
    fn index_properties(&self) -> Option<(String, Weekday, u32)> {
        // Hash by line_id, day of week, and the time of day rounded to the smallest hour

        let line_id = if let Some(line_code) = &self.trip_id {
            line_code.chars().take(7).collect::<String>()
        } else {
            return None;
        };
        let weekday = if let LocalResult::Single(timestamp) =
            Utc.timestamp_millis_opt(self.timestamp)
        {
            timestamp.weekday()
        } else {
            return None;
        };

        let hours = if let LocalResult::Single(timestamp) =
            Utc.timestamp_millis_opt(self.timestamp)
        {
            timestamp.time()
        } else {
            return None;
        };

        Some((line_id, weekday, hours.hour()))
    }
}

impl StopPrediction {
    fn index_properties(&self) -> Option<(String, Weekday, u32)> {
        // Hash by stop_id, day of week, and the time of day rounded to the smallest hour

        // TODO THIS IS HARD CODED, CHANGE IT
        let stop_id = "172524".to_string();
        // TODO get better way of getting weekday
        let weekday = chrono::offset::Local::now().weekday();
        // get from estimatedArrivalTime field with format hh:mm:ss
        let hour_of_day = if let time_str = &self.scheduled_arrival {
            let time = if let Ok(time) =
                chrono::NaiveTime::parse_from_str(time_str, "%H:%M:%S")
            {
                time.hour()
            } else {
                return None;
            };
            time
        } else {
            return None;
        };
        return Some((stop_id, weekday, hour_of_day));
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let state = Arc::new(State {
        user_preferences: RwLock::new(HashMap::new()),
        position_map: RwLock::new(HashMap::new()),
        prediction_map: RwLock::new(HashMap::new()),
        bus_progress: RwLock::new(HashMap::new()),
    });

    let stop_predictions: Vec<api_data::StopPrediction> = fetch_if_not_cached(
        "https://api.carrismetropolitana.pt/stops/172524/realtime",
    )
    .await?;
    let vehicle_positions: Vec<api_data::VehicleEvent> =
        fetch_if_not_cached("https://api.carrismetropolitana.pt/vehicles")
            .await?;

    let stop_list: Vec<api_data::Stop> = fetch_if_not_cached(
        "https://schedules.carrismetropolitana.pt/api/stops",
    )
    .await?;

    let stop_routes = stop_list
        .iter()
        .map(|stop| {
            (
                stop.stop_id.parse::<_>().unwrap(),
                stop.routes
                    .iter()
                    .map(|route| route.route_id.clone())
                    .collect_vec(),
            )
        })
        .collect::<HashMap<StopID, Vec<RouteID>>>();

    let stops: HashMap<StopID, Arc<state::Stop>> = stop_list
        .into_iter()
        .map(|stop| (stop.stop_id.parse::<_>().unwrap(), Arc::new(stop.into())))
        .collect::<HashMap<_, _>>();

    let shapes = read_shapes();

    let routes = tml_gtfs_route_trips(&stops, &shapes)?;

    let trips: HashMap<String, Trip> = routes
        .into_iter()
        .flat_map(|(key, bus_route)| {
            bus_route.trips.into_iter().map(|trip| {
                (
                    {
                        let t_id = trip.id.clone();
                        if trip.id.starts_with("p") {
                            t_id.chars().skip(3).collect()
                        } else {
                            t_id
                        }
                    },
                    trip,
                )
            })
        })
        .collect();
    // {
    //     let mut bus_prog = state.bus_progress.write().unwrap();
    //     routes.iter().for_each(|(key, bus_route)| {
    //         bus_route.trips.into_iter().for_each(|trip| {
    //             bus_prog.insert(trip.id, BusProgress::new(Arc::new(trip.clone())));
    //         })
    //     });
    //     println!("{:?}",bus_prog.keys())
    // }

    // dbg!(trips.keys());
    {
        let mut bus_prog = state.bus_progress.write().unwrap();
        vehicle_positions.into_iter().for_each(|vehicle_event| {
            let trip_id = vehicle_event.trip_id;
            match trip_id {
                Some(trip_id) => {
                    let trip = trips.get(&*trip_id);
                    if let Some(trip) = trip {
                        // println!("{:?}", trip_id);
                        bus_prog
                            .entry(trip_id)
                            .or_insert(BusProgress::new(Arc::new(trip.clone())))
                            .update_position(
                                vehicle_event.lon,
                                vehicle_event.lat,
                            );
                        // bus_prog.insert(trip_id, BusProgress::new(Arc::new(trip.unwrap().clone())));
                    }
                }
                None => {}
            }
            // dbg!(vehicle_event);
        })
    }

    // let stop_ids = stop_list
    //     .iter()
    //     .filter(|stop| !stop.routes.is_empty())
    //     .map(|stop| stop.stop_id.clone())
    //     .collect::<Vec<_>>();

    let user_preferences: Vec<UserPreference> = (0..10_000)
        .into_par_iter()
        .map(|i| {
            let mut rng: SmallRng = SmallRng::seed_from_u64(SEED + i);
            Preference::gen_n_random(10, &mut rng, &stop_routes)
        })
        .collect::<Vec<_>>();

    user_preferences.iter().for_each(|pref| {
        let mut position_map = state.position_map.write().unwrap();
        let mut prediction_map = state.prediction_map.write().unwrap();
        for p in pref {
            // println!("{:?}\n", p);
            match p {
                Preference::Position { data, distance } => {
                    for hours in 0..=data.duration.num_hours() {
                        let time = data.start_time + Duration::hours(hours);
                        // TODO rust safety stuff unwrap bad
                        position_map
                            .entry((
                                data.route_id.clone(),
                                time.weekday(),
                                time.hour(),
                            ))
                            .or_default()
                            .push((distance.clone(), data.clone()));
                    }
                }
                Preference::Prediction { data, duration } => {
                    for hours in 0..=data.duration.num_hours() {
                        let time = data.start_time + Duration::hours(hours);
                        // TODO rust safety stuff unwrap bad
                        prediction_map
                            .entry((
                                data.route_id.clone(),
                                time.weekday(),
                                time.hour(),
                            ))
                            .or_default()
                            .push((duration.clone(), data.clone()));
                    }
                }
            }
        }
    });

    // vehicle_positions.iter().for_each(|pos| {
    //     state.bus_progress.write().unwrap().insert(
    //         pos.id.clone(),
    //         BusProgress {

    //         },
    //     );
    // });

    Ok(())

    //
    // let addr = SocketAddr::from(([0, 0, 0, 0], 6969));
    //
    // axum::Server::bind(&addr)
    //     .serve(build_paths(state).into_make_service())
    //     .await
    //     .expect("Unable to start service");
}
