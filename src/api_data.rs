use serde::{Deserialize, Serialize};
use serde_this_or_that::as_f64;

#[derive(Serialize, Deserialize, Debug)]
pub(crate) struct StopPrediction {
    pub line_id: String,
    pub pattern_id: String,
    pub route_id: String,
    pub trip_id: String,
    pub headsign: String,
    pub stop_sequence: i64,
    pub scheduled_arrival: String,
    pub observed_arrival: Option<String>,
    pub vehicle_id: Option<String>,
    pub estimated_arrival: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub(crate) struct VehicleEvent {
    pub id: String,
    pub lat: f64,
    pub lon: f64,
    pub speed: i64,
    pub timestamp: i64,
    pub heading: f64,
    pub trip_id: Option<String>,
    pub pattern_id: Option<String>,
}
#[derive(Serialize, Deserialize, Clone)]
pub struct Route {
    pub route_id: String,
    pub route_short_name: String,
    pub route_long_name: String,
    pub route_color: String,
    pub route_text_color: String,
    pub _id: String,
}

impl Default for Route {
    fn default() -> Self {
        Self {
            route_id: "".to_string(),
            route_short_name: "".to_string(),
            route_long_name: "".to_string(),
            route_color: "".to_string(),
            route_text_color: "".to_string(),
            _id: "".to_string(),
        }
    }
}

#[derive(Serialize, Deserialize)]
pub(crate) struct Stop {
    pub _id: String,
    pub stop_id: String,
    pub stop_name: String,
    // TODO drop once the API returns floats
    #[serde(deserialize_with = "as_f64")]
    pub stop_lat: f64,
    #[serde(deserialize_with = "as_f64")]
    pub stop_lon: f64,
    pub routes: Vec<Route>,
    #[serde(rename = "createdAt")]
    pub created_at: String,
    #[serde(rename = "updatedAt")]
    pub updated_at: String,
}
