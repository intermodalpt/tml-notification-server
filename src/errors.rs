use std::fmt;
use std::fmt::{Debug, Display, Formatter};

#[derive(Debug, Eq, PartialEq, Hash, Clone)]
pub enum Error {
    StopNotFound,
    ShapeNotFound,
    TripNotFound,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!("{:?}", self))
    }
}

impl std::error::Error for Error {}
